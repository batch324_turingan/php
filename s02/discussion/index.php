<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>s02: Selection Control Structure and Array Manipulation</title>
</head>
<body>

	<h1>Repetition Control Structures</h1>
	<h2>While Loop</h2>
	<?php whileLoop(); ?>
	<?php whileLoop1(10); ?>

	<h2>Do-While Loop</h2>
	<?php doWhileLoop(); ?>

	<h2>For Loop</h2>
	<?php forLoop(); ?>

	<h2>Modified For Loop</h2>
	<?php modifiedForLoop(); ?>

	<h1>Array Manipulation</h1>

	<h2>Types of Arrays</h2>
	<h3>Simple Array</h3>

	<ul>
		<!-- PHP includes a short hand for "php echo tag" -->
	    <?php foreach ($computerBrands as $brand) {?>
	        <li><?= $brand; ?></li>
	    <?php } ?>
	</ul>

	<h3>Associative Array</h3>
	<ul>
		<?php foreach($gradePeriods as $period => $grade) { ?>
			<li>
				<!-- The echo and the other part is the same. -->
				Grade in <?php echo $period; ?> is <?= $grade ?>
			</li>
		<?php } ?>
	</ul>

	<h3>Multidimensional Array</h3>
	<ul>
		<?php
			foreach($heroes as $team){
				foreach($team as $member){
					?>
						<li><?php echo $member ?></li>
					<?php
				}
			}
		?>
	</ul>

	<h3>Multi-Dimensional Associative Array</h3>
	<ul>
		<?php 
			foreach($ironManPowers as $label => $powerGroup){
				foreach($powerGroup as $power){
					?>
						<li><?="$label: $power"; ?></li>
					<?php
				}
			}
		?>
	</ul>

	<h1>Mutators</h1>
	<h3>Sorting</h3>
	<p><?php print_r($sortedBrands); ?></p>
	<p><?php print_r($reverseSortedBrands); ?></p>

	<h3>PUSH</h3>
	<?php array_push($computerBrands, 'Apple'); ?>
	<p><?php print_r($computerBrands); ?></p>

	<h3>Unshift</h3>
	<?php array_unshift($computerBrands, 'Dell'); ?>
	<p><?php print_r($computerBrands); ?></p>

	<h3>Pop</h3>
	<?php array_pop($computerBrands); ?>
	<p><?php print_r($computerBrands); ?></p>

	<h3>Shift</h3>
	<?php array_shift($computerBrands); ?>
	<p><?php print_r($computerBrands); ?></p>

	<h3>Count</h3>
	<p><?php echo count($computerBrands); ?></p>

	<h3>In array</h3>
	<p><?php var_dump(in_array('Acer', $computerBrands)); ?></p>
	
</body>
</html>