<?php 

/*Section: Comments are meant to describe the algorithm of the written code.

Two types of comments:
	- The single-line comment denoted by two forward slahes (//)
	- The multi-line comment denoted by a slash and asterisk (/*)
*/

/*Section - Variables
	
	- Variables are used to contain data.
	- Variables are named location for the stored value.
	- Variables are defined using the dollar ($) notation before the name of the variable.
*/	
// $trial = "Hello World";

$name = "Roque Jerico Turingan";
$email = "bamboo.jtrakista07@gmail.com";

/*Section - Constant Variable
	- Used to hold data that are meant to be read-only.
	- Defined using the defined() / function.

	Syntax:
	define('variableName', valueofTheVariable);
*/
define('PI', 3.1416);

/*Section - Data Types in PHP
	1.Strings 
*/

// Strings :
$state = 'New York';
$country = 'United States of America';
$address = $state.', '. $country;

// Reassign the address
$address = "$state, $country.";

// Integer
$age = 31;
$headcount = 26;

// Floats
$grade = 98.2;
$distanceInKilometers = 1342.12;

// Boolean
$hasTravelAbroad = false;
$haveSymptoms = true;

// Null
$spouse = null;
$middle = null;

// Array
$grades = array(98.7, 92.1, 90.2, 94.6);

// Objects
$gradesObj = (object)[
	'firstGrading' => 98.7,
	'secondGrading' => 92.1,
	'thirdGrading' => 90.2,
	'fourthGrading' => 94.6
];

$personObj = (object)[
	'fullName' => 'Monica Ocampo',
	'isMarried' => false,
	'age' => 21,
	'address' => (object)[
		'state' => 'California',
		'country' => 'United States of America'
	],
	'contacts' => array('09186701522', '09228822828')
];

/*Section - Operators
	1. Assignment Operators(=)
		- This operator is used to assign and reassign values of a variable. 
*/

// Assignment Operator
$x = 1342.14;
$y = 1268.24;
$isLegalAge = true;
$isRegistered = false;

/*Section - Functions
	- used to make reusable code.
*/

function getFullName($firstName, $middleInitial, $lastName){
	// You can add here your algorithm

	return "$lastName, $firstName $middleInitial";
}

/*Section - Selection Control Structure
	- are used to make code execution dynamic according to predefined conditions.
	Examples are: if-elseif-else statement
*/

function determineTyphoonIntensity($windSpeed){
	if($windSpeed < 30){
		return 'Not a typhoon yet';
	} else if ($windSpeed <= 61){
		return 'Tropical depression detected.';
	} else if ($windSpeed <= 88){
		return 'Tropical Storm detected';
	} else if ($windspeed <= 117){
		return 'Severe Tropical Storm Detected';
	} else {
		return 'Typhoon detected';
	}
}

// Conditional (Ternary) Operator

function isUnderAge($age){
	return ($age < 18 ) ? true : false;
}

// Switch Statement

function determineComputerUser($computerNumber){
	switch($computerNumber){
		case 1:
			return 'Linus Torvalds';
			break;
		case 2:
			return 'Steve Jobs';
			break;
		case 3:
			return 'Sid Meir';
			break;
		case 4:
			return 'Onel de Guzman';
			break;
		case 5:
			return 'Christian Salvador';
			break;
		default:
			return $computerNumber. 'is out of bounds.';
			break;
	}
}

// Try-Catch-Finally Statement

function greeting($str){
	try{
		// Attemp to execute the code
		if (gettype($str) == "string"){
			// If Successful
			echo $str;
		} else {
			// If not, it will return "Oops"
			throw new Exception("Oops!");
		}
	}
	catch (Exception $e) {
		// Catch the error within try
		echo $e->getMessage();
	}
	finally {
		// Continue execution of code regardless of success and failure of code in execution in 'try' block.
		echo "I did it again";
	}
}

?>