<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>s01: PHP Basics and Selection Control structure</title>
</head>

<body>
	<h1>Echoing Values</h1>

	<!-- Single quote for the echo -->
	<p><?php echo 'Good day $name! Your given email is $email.';?></p>

	<!-- Double quote for the echo -->
	<p><?php echo "Good day $name! Your given email is $email."; ?></p>

	<!-- Using constant variable -->
	<p><?php echo PI; ?></p>

	<!-- Example of Data Types -->
	<!-- String -->
	<p><?php echo $state; ?></p>
	<p><?php echo $country; ?></p>
	<p><?php echo $address; ?></p>

	<!-- Integers -->
	<p><?php echo $age; ?></p>
	<p><?php echo $headcount; ?></p>

	<!-- Floats -->
	<p><?php echo $grade; ?></p>
	<p><?php echo $distanceInKilometers; ?></p>

	<!-- Boolean -->
	<!-- Normal echoing of boolean variables will not make it visible to the web page. So use the var_dump. -->
	<p><?php echo var_dump($hasTravelAbroad); ?></p>
	<p><?php echo $haveSymptoms; ?></p>

	<!-- Null -->
	<p><?php echo var_dump($spouse); ?></p>
	<p><?php echo $middle; ?></p>

	<!-- Arrays -->
	<p><?php echo $grades[0]; ?></p>
	<p><?php echo print_r($grades); ?></p>

	<!-- Objects -->
	<p><?php echo print_r($gradesObj); ?></p>
	<p><?php echo $gradesObj->firstGrading; ?></p>

	<!-- Nested Object -->
	<p><?php echo  print_r ($personObj); ?></p>
	<p><?php echo $personObj->address->state; ?></p>
	<p><?php echo $personObj->contacts[0]; ?></p>

	<h3>gettype()</h3>
	<p><?php echo gettype($state); ?></p>
	<p><?php echo gettype($grades); ?></p>

	<!-- Operators -->
	<h3>Assignment Operator</h3>
	<p>X: <?php echo $x; ?></p>
	<p>Y: <?php echo $y; ?></p>
	<p>Is Legal Age: <?php echo var_dump($isLegalAge); ?></p>
	<p>Is Registered: <?php echo var_dump($isRegistered); ?></p>

	<!-- Arithmetic Operators -->
	<p>Sum: <?php echo $x + $y; ?></p>
	<p>Difference: <?php echo $x - $y; ?></p>
	<p>Product: <?php echo $x * $y; ?></p>
	<p>Quotient: <?php echo $x / $y; ?></p>
	<p>Modulo: <?php echo $x % $y; ?></p>

	<!-- Equality Operators -->
	<p>Loose Equality: <?php echo var_dump($x == '1342.14'); ?></p>
	<p>Strict Equality: <?php echo var_dump($x === '1342.14'); ?></p>

	<p>Loose Inequality: <?php echo var_dump($x != '1342.14'); ?></p>
	<p>Strict Inequality: <?php echo var_dump($x !== '1342.14'); ?></p>
	<!-- Strict Equality/Inequaity is preffered so that we can check both of the value and the data type given. -->

	<!-- Greater/Lesser Operators -->
	<p>Is Lesser: <?php echo var_dump($x < $y); ?></p>
	<p>Is Greater: <?php echo var_dump($x > $y); ?></p>

	<p>Is Lesser or Equal: <?php echo var_dump($x <= $y); ?></p>
	<p>Is Greater or Equal: <?php echo var_dump($x >= $y); ?></p>

	<!-- Logical Operators -->

	<p>OR operator: <?php echo var_dump($isLegalAge || $isRegistered); ?></p>
	<p>AND operator: <?php echo var_dump($isLegalAge && $isRegistered); ?></p>
	<p>NOT operator: <?php echo var_dump(!$isLegalAge); ?></p>

	<!-- Function -->
	<h1>Function</h1>
	<p><?php echo getFullName('John', 'D.', 'Smith'); ?></p>

	<!-- Selection Control Structures -->
	<h1>Selection Control Structures</h1>
	<h3>If-Else If-Else Statement</h3>
	<p><?php echo determineTyphoonIntensity(39); ?></p>
	
	<h2>Ternary Sample (Is Underage?)</h2>
	<p>78:	<?php echo var_dump(isUnderAge(78)); ?></p>
	<p>17:	<?php echo var_dump(isUnderAge(17)); ?></p>

	<h2>Switch Statement</h2>
	<p><?php echo determineComputerUser(5); ?></p>

	<h2>Try-Catch-Finally</h2>
	<p><?php echo greeting("12"); ?></p>

</body>
</html>