<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Activity 1 - PHP</title>
</head>
<body>

	<h2>Full Address</h2>
	<p><?php echo getFullAddress("3F Caswyn Bldg.", "Timog Avenue", "Quezon City, Metro Manila", "Philippines"); ?></p>

	<p><?php echo getFullAddress("3F Enzo Bldg.", "Buendia Avenue", "Makati City, Metro Manila", "Philippines") ?></p>

	<h2>Letter-Based Grading</h2>
	<p>87 is equivalent to <?php echo getLetterGrade(87); ?></p>
	<p>94 is equivalent to <?php echo getLetterGrade(94); ?></p>
	<p>74 is equivalent to <?php echo getLetterGrade(74); ?></p>

</body>
</html>