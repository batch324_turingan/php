<?php 

/*ACTIVITY 1*/

function getFullAddress($specificAddress, $province, $city, $country){
	return $specificAddress. ', '. $province. ', '. $city. ', '. $country;
};

/*ACTIVITY 2*/

function getLetterGrade($grade) {
    if ($grade >= 98) {
        return "A+";
    } else if ($grade >= 95) {
        return "A";
    } else if ($grade >= 92) {
        return "A-";
    } else if ($grade >= 89) {
        return "B+";
    } else if ($grade >= 86) {
        return "B";
    } else if ($grade >= 83) {
        return "B-";
    } else if ($grade >= 80) {
        return "C+";
    } else if ($grade >= 77) {
        return "C";
    } else if ($grade >= 75) {
        return "C-";
    } else {
        return "F";
    }
}

?>