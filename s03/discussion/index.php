<?php require_once "./code.php"?>


<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>S03 : Classes and Objects</title>

    </head>
    <body>

        <!-- <h1><?php echo $hello?></h1> -->
        <h1>Objects from Variables</h1>
        <p><?php echo $buildingObj->name;?></p>
        <p><?php echo $buildingObj->address->city. " " .$buildingObj->address->country;?></p>
        <!-- You cannot echo a object alone instead use var_dump -->
        <p><?php var_dump($buildingObj) ?></p>


        <h1>Object from Class</h1>
        <p><?php var_dump($building)?></p>
        <p><?php echo $building->name?></p>
        <p><?php echo $building->printName() ?></p>
        <p><?php echo $building->checkFloors() ?></p>
        <p><?php echo $secondBuilding->printName() ?></p>


        <h1>Inheritance</h1>
        <p><?php var_dump($condominium) ?></p>
        <p><?php echo $condominium->checkFloors() ?></p>
        <p><?php echo $condominium->printName() ?></p>



    </body>
</html><?php require_once "./code.php"?>


<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>S03 : Classes and Objects</title>

    </head>
    <body>

        <!-- <h1><?php echo $hello?></h1> -->
        <h1>Objects from Variables</h1>
        <p><?php echo $buildingObj->name;?></p>
        <p><?php echo $buildingObj->address->city. " " .$buildingObj->address->country;?></p>
        <!-- You cannot echo a object alone instead use var_dump -->
        <p><?php var_dump($buildingObj) ?></p>


        <h1>Object from Class</h1>
        <p><?php var_dump($building)?></p>
        <p><?php echo $building->name?></p>
        <p><?php echo $building->printName() ?></p>
        <p><?php echo $building->checkFloors() ?></p>
        <p><?php echo $secondBuilding->printName() ?></p>


        <h1>Inheritance</h1>
        <p><?php var_dump($condominium) ?></p>
        <p><?php echo $condominium->checkFloors() ?></p>
        <p><?php echo $condominium->printName() ?></p>

        <h1>Abstraction</h1>
        <p><?php var_dump($kopiko); ?></p>
        <p><?php echo $kopiko->getDrinkName(); ?></p>



    </body>
</html>