<?php

class Building {
	private $name;
	private $floors;
	private $address;

	public function __construct($name, $floors, $address){
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}

	// getter function of property name
	public function getName(){
		return $this->name;
	}

	// Setter function of property name
	public function setName($name){
		$this->name = $name;
	}

	public function getFloors(){
		return $this->floors;
	}
}

class Condominium extends Building{

};

$building = new Building('Trial Building', 9, 'Manila City, Manila');

$condominium = new Condominium('Trial Condominium', 100, 'Quezon City, Manila');

class Drink {
	protected $name;

	public function __construct($name){
		$this->name = $name;
	}

	// getter for the name property
	public function getName(){
		return $this->name;
	}

	public function setName($name){
		$this->name = $name;
	}
}

$milk = new Drink('Alaska');

class Coffee extends Drink {

}

$kopiko = new Coffee('Kopiko');


?>