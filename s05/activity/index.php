<?php require_once('server.php'); ?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Activity 5</title>
</head>
<body>

<?php if (isLoggedIn()): ?>
    <p>Welcome, <?php echo htmlspecialchars($_SESSION['username']); ?>!</p>
    <form method="post" action="index.php">
        <input type="submit" name="logout" value="Logout">
    </form>
<?php else: ?>
    <form method="post" action="index.php">
        <label for="username">Username:</label>
        <input type="text" id="username" name="username" required>
        <label for="password">Password:</label>
        <input type="password" id="password" name="password" required>
        <input type="submit" value="Login">
    </form>
    <?php if (isset($errorMessage)): ?>
        <p><?php echo htmlspecialchars($errorMessage); ?></p>
    <?php endif; ?>
<?php endif; ?>

</body>
</html>
