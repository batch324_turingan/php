<?php
// Initialize the session
session_start();

// Define valid credentials
$validUsername = 'johnsmith@gmail.com';
$validPassword = '1234';

// Function to check if user is logged in
function isLoggedIn() {
    return isset($_SESSION['username']);
}

// Logout functionality
if (isset($_POST['logout'])) {
    session_unset();
    session_destroy();
    header("Location: index.php");
    exit; // Terminate script after redirect
}

// Check if the form is submitted
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $username = $_POST['username'];
    $password = $_POST['password'];

    // Check if credentials are valid
    if ($username === $validUsername && $password === $validPassword) {
        $_SESSION['username'] = $username;
    } else {
        $errorMessage = 'Invalid credentials';
    }
}
?>
